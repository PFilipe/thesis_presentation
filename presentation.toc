\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Introduction}{4}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Logics of subformula inclusion and infectious PNmatrices}{34}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{$\forall \mathsf {Thm}$ \& $ \mathsf {Max}^{\vdash }$}{41}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{$\exists \mathsf {Thm}$ \& $ \mathsf {Min}^{\vdash }$}{45}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{1}{From counter-machines to Nmatrices}{53}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{$\mathsf {EqThm}$ and $\mathsf {Eq}^{\vdash }$}{74}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {6}{$\mathsf {Mon}$}{79}{0}{6}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {7}{Summary}{82}{0}{7}
